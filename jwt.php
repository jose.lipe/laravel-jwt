<?php

// Definindo informações

// header
$cabecalho = [
  'alg' => 'HS256',
  'type' => 'JWT'
];

// payload
$corpoDaInformacao = [
  'first_name' => 'José',
  'last_name' => 'Silva',
  'email' => 'jose@user.com'
];

$cabecalho = json_encode($cabecalho);
$corpoDaInformacao = json_encode($corpoDaInformacao);

echo "Cabeçalho JSON: $cabecalho <br>";
echo "Corpo da Informação JSON: $corpoDaInformacao <br>";

// Transformar em BASE64

$cabecalho = base64_encode($cabecalho);
$corpoDaInformacao = base64_encode($corpoDaInformacao);

echo "<br><br><br>";
echo "Cabeçalho BASE64: $cabecalho <br>";
echo "Corpo da Informação BASE64: $corpoDaInformacao <br>";

$chave = 'lhsgtyeb009jdjhdoeop99';

// Gerar assinatura

$assinatura = hash_hmac('sha256', $cabecalho.'.'.$corpoDaInformacao, $chave, true);

echo "<br><br><br>";
echo "Assinatura: $assinatura";

$assinatura = base64_encode($assinatura);

echo "<br><br><br>";
echo "Assinatura BASE64: $assinatura";

// Criar Token

echo "<br><br><br>";
echo "Token: " . $cabecalho . '.' . $corpoDaInformacao . '.' . $assinatura;


