<?php

use \Namshi\JOSE\SimpleJWS;

require __DIR__ . '/vendor/autoload.php';

$username = isset($_POST['username']) ? $_POST['username'] : "";
$password = isset($_POST['password']) ? $_POST['password'] : "";
$privateKey = '876gshs898hsbskk66';

if($username == 'jose@user.com' && $password == 'secret') {
    // login válido

    $id = 1;
    $firstName = 'José';
    $lastName = 'Da Silva';

    $jws = new SimpleJWS([
        'alg' => 'HS256'
    ]);

    $jws->setPayload([
        'id' => $id,
        'first_name' => $firstName,
        'last_name' => $lastName
    ]);

    $jws->sign($privateKey);
    echo json_encode(['token' => $jws->getTokenString()]);
}else {
    try {
        $jws = SimpleJWS::load($_GET['token']);
    }catch(\Exception $e) {
        echo "seu token é inválido";
        return;
    }

    if($jws->isValid($privateKey)) {
        echo "seu token é válido";
    }else {
        echo "este token não é compatível";
    }
}
